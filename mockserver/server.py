#!/usr/bin/env python3

import asyncio
import json
import random
import time
from pprint import pformat

import websockets


PORT = 8765
ENVWIDTH = 600


class State:
    resting = "resting"
    trial = "trial"


DURATIONS = {
    State.resting: 2,
    State.trial: 500
}


class InEvents:
    sub = "sub"
    unsub = "unsub"
    ready = "ready"


class OutEvents:
    state = "state"
    positions = "positions"
    error = "error"


SUBS = {OutEvents.state: set(), OutEvents.positions: set()}
STATE = State.resting


async def error(websocket, message):
    if not isinstance(message, str):
        message = pformat(message)

    event = {
        "tipe": OutEvents.error,
        "data": message,
    }
    await websocket.send(json.dumps(event))


def state_event():
    state = {
        "state": STATE,
        "duration": DURATIONS[STATE]
    }
    if STATE == State.trial:
        state["config"] = {
            "envwidth": ENVWIDTH,
        }
        # TODO: take this from real positions
        state["positions"] = {
            "p0": 150,
            "p1": 450
        }

    return {
        "tipe": OutEvents.state,
        "data": state
    }


def broadcast_state():
    websockets.broadcast(SUBS[OutEvents.state], json.dumps(state_event()))


async def game():
    global STATE
    while True:
        STATE = State.resting
        broadcast_state()
        await asyncio.sleep(DURATIONS[STATE])

        STATE = State.trial
        broadcast_state()
        positions = {"p0": 150, "p1": 450}

        rate = 25
        loop_end_time = time.time()
        loop_start_time = loop_end_time
        for i in range(DURATIONS[STATE] * rate):
            prev_loop_start_time = loop_start_time
            loop_start_time = time.time()
            print(f"Game loop time: {loop_start_time - prev_loop_start_time}")

            event = {
                "tipe": OutEvents.positions,
                "data": positions
            }
            websockets.broadcast(SUBS[OutEvents.positions], json.dumps(event))
            positions["p0"] = (positions["p0"] + random.randint(-10, 5)) % ENVWIDTH
            positions["p1"] = (positions["p1"] + random.randint(-5, 10)) % ENVWIDTH
            start_sleep_time = time.time()
            await asyncio.sleep(1 / rate - (start_sleep_time - loop_end_time))
            loop_end_time = time.time()


async def process_message(websocket, message, expected_tipe=None):
    try:
        event = json.loads(message)
        if expected_tipe is not None:
            if event["tipe"] != expected_tipe:
                await error(websocket, f'Wrong initial event type {event["tipe"]}, expected {expected_tipe}')
                return False

        if event["tipe"] == InEvents.sub:
            for channel in event["data"]:
                SUBS[channel].add(websocket)
        elif event["tipe"] == InEvents.unsub:
            for channel in event["data"]:
                SUBS[channel].discard(websocket)
        elif event["tipe"] == InEvents.ready:
            print(f"Ready message from {websocket}: {event}")
        else:
            await error(websocket, f'Unknown event type: {event["tipe"]}')
            return False

    except json.JSONDecodeError as e:
        await error(websocket, e)
        return False

    except Exception as e:
        await error(websocket, f"An error occured: {pformat(e)}")
        return False

    return True


async def handler(websocket):
    print(f"New connection: {websocket.remote_address}")

    try:
        message = await websocket.recv()
        if not await process_message(websocket, message, expected_tipe=InEvents.sub):
            return

        await websocket.send(json.dumps(state_event()))

        async for message in websocket:
            await process_message(websocket, message)

    finally:
        for subs in SUBS.values():
            subs.discard(websocket)
        print(f"Connection closed: {websocket.remote_address}")


async def main():
    async with websockets.serve(handler, "localhost", PORT):
        print(f"Websocket open at port {PORT}")
        await game()


if __name__ == "__main__":
    asyncio.run(main())
