#!/usr/bin/env bash

inotifywait -m -e close_write -r src | while read -r line; do
    elm make --debug src/Main.elm --output=assets/elm.js
done
