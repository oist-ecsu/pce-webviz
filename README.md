PCE Web visualiser
==================

Requirements
------------

- [Elm](https://elm-lang.org/) 0.19.1
- Python 3
- [pipenv](https://pipenv.pypa.io/en/latest/)


Usage
-----

1. Build the web app: `elm make --debug src/Main.elm --output=assets/elm.js`
2. Serve the web app: `python3 -m http.server` (here `python3` is your system's Python 3)
3. Run the mock server (or start the RPi PCE script):
   ```
   cd mockserver
   pipenv install --dev
   pipenv run python server.py  # here python is the pipenv's Python 3
   ```
4. Open [localhost:8000](http://localhost:8000/)

Development
-----------

Run all four steps above, and:
```
inotifywait -m -e close_write -r src | while read line; elm make --debug src/Main.elm --output=assets/elm.js; end
```
to continuously build.
