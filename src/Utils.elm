module Utils exposing (..)

import Dict
import Http
import List
import Types exposing (..)


getPProp : P -> PProps a -> a
getPProp p { p0, p1 } =
    case p of
        P0 ->
            p0

        P1 ->
            p1


mapPProps : (a -> b) -> PProps a -> PProps b
mapPProps f props =
    { p0 = f props.p0
    , p1 = f props.p1
    }


dictToString : Dict.Dict String String -> String
dictToString dict =
    Dict.toList dict
        |> List.map (\( k, v ) -> "\"" ++ k ++ "\": \"" ++ v ++ "\"")
        |> String.join ", "


ownSlot : Meta -> Maybe Slot
ownSlot meta =
    if Just meta.uuid == meta.slots.experimenter then
        Just Experimenter

    else if Just meta.uuid == meta.slots.p0 then
        Just <| Participant P0

    else if Just meta.uuid == meta.slots.p1 then
        Just <| Participant P1

    else
        Nothing


httpErrorToString : Http.Error -> String
httpErrorToString error =
    case error of
        Http.BadUrl msg ->
            "BadUrl: " ++ msg

        Http.Timeout ->
            "Timeout"

        Http.NetworkError ->
            "NetworkError"

        Http.BadStatus code ->
            "BadStatus: " ++ String.fromInt code

        Http.BadBody msg ->
            "BadBody: " ++ msg


slotToString : Slot -> String
slotToString slot =
    case slot of
        Experimenter ->
            "experimenter"

        Participant P0 ->
            "p0"

        Participant P1 ->
            "p1"


pToString : P -> String
pToString p =
    case p of
        P0 ->
            "Participant 0"

        P1 ->
            "Participant 1"


nextPhaseToString : NextPhase -> String
nextPhaseToString nextPhase =
    case nextPhase of
        TrialPhase (Just Visible) ->
            "test trial with visible space"

        TrialPhase (Just Hidden) ->
            "test trial with hidden space"

        TrialPhase Nothing ->
            "trial"

        RestingPhase ->
            "resting state"
