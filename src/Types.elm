module Types exposing (..)

import Dict
import Dropdown
import Feedback exposing (Feedback)
import Form
import Http
import Json.Decode as D



-- MESSAGES


type Msg
    = NoOp
    | Errored String
    | RecvMeta (Result Http.Error Meta)
    | SocketState (Result String Bool)
    | RecvSocketMsg (Result D.Error SocketMsg)
    | SelectSlot Slot
    | RecvSlots (Result Http.Error (ApiResponse Slots))
    | PidInput String
    | RecvPids (Result Http.Error (ApiResponse Pids))
    | SubmitPid
    | SubmitStartPhase
    | RecvPhase (Result Http.Error (ApiResponse Phase))
    | SubmitReady
    | RecvPlayers (Result Http.Error (ApiResponse Players))
    | PersonalityInput Int Int
    | PersonInputAge String
    | PersonSelectGender (Maybe String)
    | PersonGenderDropdownMsg (Dropdown.Msg String)
    | PersonSelectNationality (Maybe String)
    | PersonNationalityDropdownMsg (Dropdown.Msg String)
    | SubmitPerson
    | RecvPersons (Result Http.Error (ApiResponse QPreExperiment))
    | SubmitPersonalityPre
    | RecvPersonalitiesPre (Result Http.Error (ApiResponse QPreExperiment))
    | SubmitPersonalityAfter
    | RecvPersonalitiesAfter (Result Http.Error (ApiResponse QAfterExperiment))
    | AwarenessInput Int
    | SubmitAwareness
    | RecvAwarenesses (Result Http.Error (ApiResponse QAfterTrial))
    | StrategySelfInput String
    | StrategyOtherInput String
    | StrategyCommentInput String
    | SubmitStrategy
    | RecvStrategies (Result Http.Error (ApiResponse QAfterExperiment))



-- API


type SocketMsg
    = SocketMsgError String
    | SocketMsgPhase Phase
    | SocketMsgMeta Meta
    | SocketMsgPlayers Players


type ApiResponse a
    = BadStatus Int Feedback
    | GoodStatus a



-- MODEL


type alias Model =
    { meta : Meta
    , slotForm : Form.Model ()
    , pidForm : Form.Model String
    , startPhaseForm : Form.Model ()
    , personForm : Form.Model Person
    , personalityForm : Form.Model Personality
    , awarenessForm : Form.Model Awareness
    , strategyForm : Form.Model Strategy
    , genderDropdown : Dropdown.State String
    , nationalityDropdown : Dropdown.State String
    , players : Players
    , exp : Exp
    }


type alias Meta =
    { slots : Slots
    , pids : Pids
    , uuid : Uuid
    }


type alias Slots =
    { experimenter : Maybe Uuid
    , p0 : Maybe Uuid
    , p1 : Maybe Uuid
    }


type Exp
    = Connecting
    | Error String
    | Connected Phase


type alias PProps a =
    { p0 : a
    , p1 : a
    }


type alias Pids =
    PProps (Maybe String)


type alias Player =
    { x : Float, button : Bool, ready : Bool, clicked : Bool }


type alias Players =
    PProps Player


type alias QPreExperiment =
    { person : PProps Bool
    , personalityPre : PProps Bool
    }


type alias QAfterTrial =
    { awareness : PProps Bool
    }


type alias QAfterExperiment =
    { personalityAfter : PProps Bool
    , strategy : PProps Bool
    }


type alias Uuid =
    String


type Slot
    = Experimenter
    | Participant P


type P
    = P0
    | P1


type NextPhase
    = TrialPhase (Maybe Training)
    | RestingPhase


type Phase
    = PreExperiment QPreExperiment SpaceConfig
      -- TODO: move form models to inside the phase models
    | PreFirstResting
    | Resting Duration
    | PreTrials
    | Trial Duration SpaceConfig
    | AfterResting
    | AfterTrial (Maybe Training) NextPhase QAfterTrial
    | AfterExperiment QAfterExperiment
    | End


type Training
    = Visible
    | Hidden


type alias Duration =
    Int


type alias SpaceConfig =
    { envWidth : Float
    , avatarWidth : Float
    , staticWidth : Float
    , static0 : Float
    , static1 : Float
    , shadowDelta0 : Float
    , shadowDelta1 : Float
    , training : Maybe Training
    }



-- QUESTIONNAIRES


type alias Personality =
    Dict.Dict Int ( String, Int )


personalityQuestionTexts =
    Dict.fromList
        [ ( 1, "Is outgoing, sociable" )
        , ( 2, "Is compassionate, has a soft heart" )
        , ( 3, "Tends to be disorganized" )
        , ( 4, "Is relaxed, handles stress well" )
        , ( 5, "Has few artistic interests" )
        , ( 6, "Has an assertive personality" )
        , ( 7, "Is respectful, treats others with respect" )
        , ( 8, "Tends to be lazy" )
        , ( 9, "Stays optimistic after experiencing a setback" )
        , ( 10, "Is curious about many different things" )
        , ( 11, "Rarely feels excited or eager" )
        , ( 12, "Tends to find fault with others" )
        , ( 13, "Is dependable, steady" )
        , ( 14, "Is moody, has up and down mood swings" )
        , ( 15, "Is inventive, finds clever ways to do things" )
        , ( 16, "Tends to be quiet" )
        , ( 17, "Feels little sympathy for others" )
        , ( 18, "Is systematic, likes to keep things in order" )
        , ( 19, "Can be tense" )
        , ( 20, "Is fascinated by art, music, or literature" )
        , ( 21, "Is dominant, acts as a leader" )
        , ( 22, "Starts arguments with others" )
        , ( 23, "Has difficulty getting started on tasks" )
        , ( 24, "Feels secure, comfortable with self" )
        , ( 25, "Avoids intellectual, philosophical discussions" )
        , ( 26, "Is less active than other people" )
        , ( 27, "Has a forgiving nature" )
        , ( 28, "Can be somewhat careless" )
        , ( 29, "Is emotionally stable, not easily upset" )
        , ( 30, "Has little creativity" )
        , ( 31, "Is sometimes shy, introverted" )
        , ( 32, "Is helpful and unselfish with others" )
        , ( 33, "Keeps things neat and tidy" )
        , ( 34, "Worries a lot" )
        , ( 35, "Values art and beauty" )
        , ( 36, "Finds it hard to influence people" )
        , ( 37, "Is sometimes rude to others" )
        , ( 38, "Is efficient, gets things done" )
        , ( 39, "Often feels sad" )
        , ( 40, "Is complex, a deep thinker" )
        , ( 41, "Is full of energy" )
        , ( 42, "Is suspicious of others’ intentions" )
        , ( 43, "Is reliable, can always be counted on" )
        , ( 44, "Keeps their emotions under control" )
        , ( 45, "Has difficulty imagining things" )
        , ( 46, "Is talkative" )
        , ( 47, "Can be cold and uncaring" )
        , ( 48, "Leaves a mess, doesn’t clean up" )
        , ( 49, "Rarely feels anxious or afraid" )
        , ( 50, "Thinks poetry and plays are boring" )
        , ( 51, "Prefers to have others take charge" )
        , ( 52, "Is polite, courteous to others" )
        , ( 53, "Is persistent, works until the task is finished" )
        , ( 54, "Tends to feel depressed, blue" )
        , ( 55, "Has little interest in abstract ideas" )
        , ( 56, "Shows a lot of enthusiasm" )
        , ( 57, "Assumes the best about people" )
        , ( 58, "Sometimes behaves irresponsibly" )
        , ( 59, "Is temperamental, gets emotional easily" )
        , ( 60, "Is original, comes up with new ideas" )
        ]


genderOptions : List String
genderOptions =
    [ "Female", "Male", "Non-binary" ]


nationalityOptions : List String
nationalityOptions =
    knownCountries ++ [ "Another nationality" ]


type alias Person =
    -- TODO: add a channel for comment, e.g. gender or nationality not in list, etc.
    { age : Maybe Int
    , gender : Maybe String
    , nationality : Maybe String
    }


type alias ValidPerson =
    { age : Int
    , gender : String
    , nationality : String
    }


type alias Awareness =
    { pas : Maybe Int }


type alias ValidAwareness =
    { pas : Int }


type alias Strategy =
    { self : String
    , other : String
    , comment : String
    }



-- INIT


initModel : Model
initModel =
    { meta = initMeta
    , slotForm = Form.empty ()
    , pidForm = Form.empty ""
    , startPhaseForm = Form.empty ()
    , personForm = Form.empty initPerson
    , personalityForm = Form.empty initPersonality
    , awarenessForm = Form.empty initAwareness
    , strategyForm = Form.empty initStrategy
    , genderDropdown = Dropdown.init "gender-dropdown"
    , nationalityDropdown = Dropdown.init "nationality-dropdown"
    , players = initPlayers
    , exp = Connecting
    }


initMeta : Meta
initMeta =
    { slots = initSlots
    , pids = initPids
    , uuid = ""
    }


initSlots : Slots
initSlots =
    { experimenter = Nothing, p0 = Nothing, p1 = Nothing }


initPids : Pids
initPids =
    PProps Nothing Nothing


initPlayers : Players
initPlayers =
    { p0 = { x = 0, button = False, ready = False, clicked = False }
    , p1 = { x = 0, button = False, ready = False, clicked = False }
    }


initPerson : Person
initPerson =
    { age = Nothing
    , gender = Nothing
    , nationality = Nothing
    }


initPersonality : Personality
initPersonality =
    Dict.empty


initAwareness : Awareness
initAwareness =
    { pas = Nothing }


initStrategy : Strategy
initStrategy =
    { self = ""
    , other = ""
    , comment = ""
    }



-- DATA


knownCountries : List String
knownCountries =
    [ "Abkhazia – Republic of Abkhazia"
    , "Afghanistan"
    , "Albania – Republic of Albania"
    , "Algeria – People's Democratic Republic of Algeria"
    , "Andorra – Principality of Andorra"
    , "Angola – Republic of Angola"
    , "Antigua and Barbuda"
    , "Argentina – Argentine Republic"
    , "Armenia – Republic of Armenia"
    , "Artsakh – Republic of Artsakh"
    , "Australia – Commonwealth of Australia"
    , "Austria – Republic of Austria"
    , "Azerbaijan – Republic of Azerbaijan"
    , "Bahamas, The – Commonwealth of The Bahamas"
    , "Bahrain – Kingdom of Bahrain"
    , "Bangladesh – People's Republic of Bangladesh"
    , "Barbados"
    , "Belarus – Republic of Belarus"
    , "Belgium – Kingdom of Belgium"
    , "Belize"
    , "Benin – Republic of Benin"
    , "Bhutan – Kingdom of Bhutan"
    , "Bolivia – Plurinational State of Bolivia"
    , "Bosnia and Herzegovina"
    , "Botswana – Republic of Botswana"
    , "Brazil – Federative Republic of Brazil"
    , "Brunei – Brunei Darussalam"
    , "Bulgaria – Republic of Bulgaria"
    , "Burkina Faso"
    , "Burundi – Republic of Burundi"
    , "Cambodia – Kingdom of Cambodia"
    , "Cameroon – Republic of Cameroon"
    , "Canada"
    , "Cape Verde – Republic of Cabo Verde"
    , "Central African Republic"
    , "Chad – Republic of Chad"
    , "Chile – Republic of Chile"
    , "China – People's Republic of China"
    , "Colombia – Republic of Colombia"
    , "Comoros – Union of the Comoros"
    , "Congo, Democratic Republic of the"
    , "Congo, Republic of the"
    , "Cook Islands"
    , "Costa Rica – Republic of Costa Rica"
    , "Croatia – Republic of Croatia"
    , "Cuba – Republic of Cuba"
    , "Cyprus – Republic of Cyprus"
    , "Czech Republic"
    , "Denmark, Kingdom of"
    , "Djibouti – Republic of Djibouti"
    , "Dominica – Commonwealth of Dominica"
    , "Dominican Republic"
    , "Donetsk People's Republic"
    , "East Timor – Democratic Republic of Timor-Leste"
    , "Ecuador – Republic of Ecuador"
    , "Egypt – Arab Republic of Egypt"
    , "El Salvador – Republic of El Salvador"
    , "Equatorial Guinea – Republic of Equatorial Guinea"
    , "Eritrea – State of Eritrea"
    , "Estonia – Republic of Estonia"
    , "Eswatini – Kingdom of Eswatini"
    , "Ethiopia – Federal Democratic Republic of Ethiopia"
    , "Fiji – Republic of Fiji"
    , "Finland – Republic of Finland"
    , "France – French Republic"
    , "Gabon – Gabonese Republic"
    , "Gambia, The – Republic of The Gambia"
    , "Georgia"
    , "Germany – Federal Republic of Germany"
    , "Ghana – Republic of Ghana"
    , "Greece – Hellenic Republic"
    , "Grenada"
    , "Guatemala – Republic of Guatemala"
    , "Guinea-Bissau – Republic of Guinea-Bissau"
    , "Guinea – Republic of Guinea"
    , "Guyana – Co-operative Republic of Guyana"
    , "Haiti – Republic of Haiti"
    , "Honduras – Republic of Honduras"
    , "Hungary"
    , "Iceland – Republic of Iceland"
    , "India – Republic of India"
    , "Indonesia – Republic of Indonesia"
    , "Iran – Islamic Republic of Iran"
    , "Iraq – Republic of Iraq"
    , "Ireland"
    , "Israel – State of Israel"
    , "Italy – Italian Republic"
    , "Ivory Coast – Republic of Côte d'Ivoire"
    , "Jamaica"
    , "Japan"
    , "Jordan – Hashemite Kingdom of Jordan"
    , "Kazakhstan – Republic of Kazakhstan"
    , "Kenya – Republic of Kenya"
    , "Kiribati – Republic of Kiribati"
    , "Korea, North – Democratic People's Republic of Korea"
    , "Korea, South – Republic of Korea"
    , "Kosovo – Republic of Kosovo"
    , "Kuwait – State of Kuwait"
    , "Kyrgyzstan – Kyrgyz Republic"
    , "Laos – Lao People's Democratic Republic"
    , "Latvia – Republic of Latvia"
    , "Lebanon – Lebanese Republic"
    , "Lesotho – Kingdom of Lesotho"
    , "Liberia – Republic of Liberia"
    , "Libya – State of Libya"
    , "Liechtenstein – Principality of Liechtenstein"
    , "Lithuania – Republic of Lithuania"
    , "Luhansk People's Republic"
    , "Luxembourg – Grand Duchy of Luxembourg"
    , "Madagascar – Republic of Madagascar"
    , "Malawi – Republic of Malawi"
    , "Malaysia"
    , "Maldives – Republic of Maldives"
    , "Mali – Republic of Mali"
    , "Malta – Republic of Malta"
    , "Marshall Islands – Republic of the Marshall Islands"
    , "Mauritania – Islamic Republic of Mauritania"
    , "Mauritius – Republic of Mauritius"
    , "Mexico – United Mexican States"
    , "Micronesia, Federated States of"
    , "Moldova – Republic of Moldova"
    , "Monaco – Principality of Monaco"
    , "Mongolia"
    , "Montenegro"
    , "Morocco – Kingdom of Morocco"
    , "Mozambique – Republic of Mozambique"
    , "Myanmar – Republic of the Union of Myanmar"
    , "Namibia – Republic of Namibia"
    , "Nauru – Republic of Nauru"
    , "Nepal – Federal Democratic Republic of Nepal"
    , "Netherlands, Kingdom of the"
    , "New Zealand"
    , "Nicaragua – Republic of Nicaragua"
    , "Nigeria – Federal Republic of Nigeria"
    , "Niger – Republic of the Niger"
    , "Niue"
    , "Northern Cyprus – Turkish Republic of Northern Cyprus"
    , "North Macedonia – Republic of North Macedonia"
    , "Norway – Kingdom of Norway"
    , "Oman – Sultanate of Oman"
    , "Pakistan – Islamic Republic of Pakistan"
    , "Palau – Republic of Palau"
    , "Palestine – State of Palestine"
    , "Panama – Republic of Panama"
    , "Papua New Guinea – Independent State of Papua New Guinea"
    , "Paraguay – Republic of Paraguay"
    , "Peru – Republic of Peru"
    , "Philippines – Republic of the Philippines"
    , "Poland – Republic of Poland"
    , "Portugal – Portuguese Republic"
    , "Qatar – State of Qatar"
    , "Romania"
    , "Russia – Russian Federation"
    , "Rwanda – Republic of Rwanda"
    , "Sahrawi Arab Democratic Republic"
    , "Saint Kitts and Nevis – Federation of Saint Christopher and Nevis"
    , "Saint Lucia"
    , "Saint Vincent and the Grenadines"
    , "Samoa – Independent State of Samoa"
    , "San Marino – Republic of San Marino"
    , "São Tomé and Príncipe – Democratic Republic of São Tomé and Príncipe"
    , "Saudi Arabia – Kingdom of Saudi Arabia"
    , "Senegal – Republic of Senegal"
    , "Serbia – Republic of Serbia"
    , "Seychelles – Republic of Seychelles"
    , "Sierra Leone – Republic of Sierra Leone"
    , "Singapore – Republic of Singapore"
    , "Slovakia – Slovak Republic"
    , "Slovenia – Republic of Slovenia"
    , "Solomon Islands"
    , "Somalia – Federal Republic of Somalia"
    , "Somaliland – Republic of Somaliland"
    , "South Africa – Republic of South Africa"
    , "South Ossetia – Republic of South Ossetia–the State of Alania"
    , "South Sudan – Republic of South Sudan"
    , "Spain – Kingdom of Spain"
    , "Sri Lanka – Democratic Socialist Republic of Sri Lanka"
    , "Sudan – Republic of the Sudan"
    , "Suriname – Republic of Suriname"
    , "Sweden – Kingdom of Sweden"
    , "Switzerland – Swiss Confederation"
    , "Syria – Syrian Arab Republic"
    , "Taiwan – Republic of China"
    , "Tajikistan – Republic of Tajikistan"
    , "Tanzania – United Republic of Tanzania"
    , "Thailand – Kingdom of Thailand"
    , "Togo – Togolese Republic"
    , "Tonga – Kingdom of Tonga"
    , "Transnistria – Pridnestrovian Moldavian Republic"
    , "Trinidad and Tobago – Republic of Trinidad and Tobago"
    , "Tunisia – Republic of Tunisia"
    , "Turkey – Republic of Türkiye"
    , "Turkmenistan – Republic of Turkmenistan"
    , "Tuvalu"
    , "Uganda – Republic of Uganda"
    , "Ukraine"
    , "United Arab Emirates"
    , "United Kingdom – United Kingdom of Great Britain and Northern Ireland"
    , "United States – United States of America"
    , "Uruguay – Oriental Republic of Uruguay"
    , "Uzbekistan – Republic of Uzbekistan"
    , "Vanuatu – Republic of Vanuatu"
    , "Vatican City – Vatican City State"
    , "Venezuela – Bolivarian Republic of Venezuela"
    , "Vietnam – Socialist Republic of Vietnam"
    , "Yemen – Republic of Yemen"
    , "Zambia – Republic of Zambia"
    , "Zimbabwe – Republic of Zimbabwe"
    ]
