module View exposing (genderDropdownConfig, nationalityDropdownConfig, view)

import Dict
import Dropdown
import Element as El
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Feedback
import Form
import Html exposing (Html)
import Html.Attributes as HA
import Svg as S
import Svg.Attributes as SA
import Types exposing (..)
import Utils exposing (ownSlot, pToString)


view : Model -> Html Msg
view model =
    let
        ( header, ( recordingRunning, content ) ) =
            case model.exp of
                Connecting ->
                    ( El.none
                    , ( False, [ title "Connecting to server" ] )
                    )

                Error msg ->
                    ( El.none
                    , ( False
                      , [ title "Error"
                        , El.text msg
                        ]
                      )
                    )

                Connected phase ->
                    connectedView model phase

        recordingAttrs =
            if recordingRunning then
                [ Background.color black
                , Font.color white
                ]

            else
                []
    in
    El.layout ([ Font.family [ Font.typeface "Nimbus Sans", Font.sansSerif ] ] ++ recordingAttrs) <|
        El.column
            [ El.width <| El.px 900
            , El.centerX
            , El.height El.fill
            , El.padding 40
            , El.spacing 80
            ]
            [ El.el [ El.centerX ] header
            , El.column [ El.width El.fill, El.spacing 50 ] content
            ]


roundedGrayBoxAttrs : El.Color -> List (El.Attribute msg)
roundedGrayBoxAttrs fontColor =
    [ Font.color fontColor
    , Border.width 1
    , Border.color lightGray
    , Border.rounded 5
    , El.spacing 20
    , El.padding 10
    ]


connectedView : Model -> Phase -> ( El.Element Msg, ( Bool, List (El.Element Msg) ) )
connectedView model phase =
    case ( ownSlot model.meta, model.meta.pids.p0, model.meta.pids.p1 ) of
        ( Nothing, _, _ ) ->
            ( El.none
            , ( False, slotSelectionView model phase )
            )

        ( Just Experimenter, Just pid0, Just pid1 ) ->
            ( El.column (roundedGrayBoxAttrs darkGray)
                [ El.el [ El.centerX, Font.size 20 ] <| El.text "Experimenter view"
                , El.row [ El.spacing 40 ]
                    [ El.column [ Font.size 16 ] [ El.text "Participant 0", El.el [ Font.family [ Font.monospace ] ] (El.text pid0) ]
                    , El.column [ Font.size 16 ] [ El.text "Participant 1", El.el [ Font.family [ Font.monospace ] ] (El.text pid1) ]
                    ]
                ]
            , ( False
              , connectedExperimenterView model phase
              )
            )

        ( Just Experimenter, _, _ ) ->
            ( El.text "Experimenter view"
            , ( False
              , connectedExperimenterPidsView model.meta.pids
              )
            )

        ( Just (Participant P0), Just pid0, _ ) ->
            ( El.none, connectedParticipantView P0 model phase )

        ( Just (Participant P0), Nothing, _ ) ->
            ( El.none
            , ( False
              , connectedParticipantPidView model.pidForm
              )
            )

        ( Just (Participant P1), _, Just pid1 ) ->
            ( El.none, connectedParticipantView P1 model phase )

        ( Just (Participant P1), _, Nothing ) ->
            ( El.none
            , ( False
              , connectedParticipantPidView model.pidForm
              )
            )



-- SLOT SELECTION


slotSelectionView : Model -> Phase -> List (El.Element Msg)
slotSelectionView model phase =
    let
        disabled =
            model.slotForm.status /= Form.Entering

        slots =
            model.meta.slots

        { p0, p1 } =
            model.players
    in
    [ titleStyled [ ( [ El.centerX ], "Select your slot" ) ]
    , El.row [ El.spacing 50, El.centerX ]
        [ selectableSlotView phase Experimenter Nothing (slots.experimenter /= Nothing || disabled)
        , selectableSlotView phase (Participant P0) (Just p0) (slots.p0 /= Nothing || disabled)
        , selectableSlotView phase (Participant P1) (Just p1) (slots.p1 /= Nothing || disabled)
        ]
    ]


selectableSlotView : Phase -> Slot -> Maybe Player -> Bool -> El.Element Msg
selectableSlotView phase slot maybePlayer disabled =
    let
        playerView_ =
            case maybePlayer of
                Nothing ->
                    El.none

                Just player ->
                    case phase of
                        PreExperiment _ config ->
                            El.html <| playerView config player 5

                        _ ->
                            El.none

        inner =
            case slot of
                Experimenter ->
                    El.el [ El.centerX ] <| El.text "Experimenter"

                Participant p ->
                    El.column [ El.spacing 20 ]
                        [ El.el [ El.centerX ] <| El.text (pToString p)
                        , El.el [ El.centerX ] <| playerView_
                        ]

        attrs =
            if disabled then
                roundedGrayBoxAttrs lightGray

            else
                roundedGrayBoxAttrs darkGray
                    ++ [ El.mouseOver [ Background.color (El.rgb255 237 240 243) ]
                       , El.mouseDown [ Background.color lightGray ]
                       ]
    in
    Input.button
        attrs
        { onPress =
            if disabled then
                Nothing

            else
                Just <| SelectSlot slot
        , label = inner
        }



-- PID DEFINITION


connectedExperimenterPidsView : Pids -> List (El.Element Msg)
connectedExperimenterPidsView pids =
    [ title "Waiting for Participant IDs"
    , connectedExperimenterPidView P0 pids.p0
    , connectedExperimenterPidView P1 pids.p1
    ]


connectedExperimenterPidView : P -> Maybe String -> El.Element msg
connectedExperimenterPidView p maybePid =
    El.paragraph []
        [ El.text <| pToString p ++ ": "
        , El.el [ Font.family [ Font.monospace ] ] <|
            El.text (Maybe.withDefault "Not set yet" maybePid)
        ]


connectedParticipantPidView : Form.Model String -> List (El.Element Msg)
connectedParticipantPidView { input, feedback, status } =
    let
        invalid =
            String.length input < 4
    in
    [ El.row [ El.spacing 20 ]
        -- TODO: use inputStyled everywhere
        [ inputStyled (status /= Form.Entering)
            [ Font.family [ Font.monospace ], El.spacing 20 ]
            PidInput
            input
            (Input.labelLeft [] <| El.text "Participant ID")
        , button
            (status /= Form.Entering || invalid)
            SubmitPid
            (El.text "Send")
        ]
    , El.paragraph [] [ El.text <| Feedback.getError "pid" feedback ]
    ]



-- EXPERIMENTER PHASE VIEWS


connectedExperimenterView : Model -> Phase -> List (El.Element Msg)
connectedExperimenterView { players, startPhaseForm, personalityForm } phase =
    case phase of
        PreExperiment qPreExperiment _ ->
            experimenterReadinessView
                { currentPhase = "Personality questionnaire & EEG setup"
                , explanation =
                    [ El.text "Participants should fill the personality questionnaire while the EEG caps and body electrodes are being set up. Once all is ready and the resting phase can start, move to next phase."
                    ]
                , startPhaseForm = startPhaseForm
                , required =
                    [ ( "Person questionnaire completed", qPreExperiment.person )
                    , ( "Personality questionnaire completed", qPreExperiment.personalityPre )
                    ]
                }

        PreFirstResting ->
            experimenterReadinessView
                { currentPhase = "Instructions prior to first resting phase"
                , explanation = [ El.text "Wait for participants to declare themselves ready, then start the next phase." ]
                , startPhaseForm = startPhaseForm
                , required =
                    [ ( "Ready to start", Utils.mapPProps .ready players )
                    ]
                }

        Resting duration ->
            [ subTitle "Current phase"
            , title "Resting"
            , El.text <| "Duration: " ++ String.fromInt duration ++ " seconds"
            ]

        PreTrials ->
            experimenterReadinessView
                { currentPhase = "Pre-experiment instructions"
                , explanation = [ El.text "Wait for participants to read instructions and declare themselves ready" ]
                , startPhaseForm = startPhaseForm
                , required =
                    [ ( "Ready to start", Utils.mapPProps .ready players )
                    ]
                }

        Trial duration config ->
            let
                titleText =
                    case config.training of
                        Nothing ->
                            "Trial"

                        Just Visible ->
                            "Traning trial Visible space"

                        Just Hidden ->
                            "Training trial Hidden space"
            in
            [ El.row [ El.spacing 50, El.width El.fill, El.moveLeft 150 ]
                [ El.column [ El.width <| El.px 500, El.spacing 50, El.alignTop ]
                    [ subTitle "Current phase"
                    , titleStyled [ ( [ El.centerX ], titleText ) ]
                    , El.text <| "Duration: " ++ String.fromInt duration ++ " seconds"
                    , experimenterLegendView players
                    ]
                , El.el [ El.width <| El.px 600 ] <| El.html <| spaceView config players Experimenter 1
                ]
            ]

        AfterResting ->
            experimenterReadinessView
                { currentPhase = "After-resting"
                , explanation =
                    [ El.text <| "Wait for participants to declare themselves ready. "
                    , El.text <| "The next phase is a trial."
                    ]
                , startPhaseForm = startPhaseForm
                , required =
                    [ ( "Ready to start", Utils.mapPProps .ready players )
                    ]
                }

        AfterTrial _ nextPhase qAfterTrial ->
            experimenterReadinessView
                { currentPhase = "After-trial questionnaire"
                , explanation =
                    [ El.text <| "Wait for participants to fill the awareness questionnaire, read the instructions and declare themselves ready. "
                    , El.text <| "The next phase is a " ++ Utils.nextPhaseToString nextPhase ++ "."
                    ]
                , startPhaseForm = startPhaseForm
                , required =
                    [ ( "Questionnaire completed", qAfterTrial.awareness )
                    , ( "Ready to start", Utils.mapPProps .ready players )
                    ]
                }

        AfterExperiment qAfterExperiment ->
            experimenterReadinessView
                { currentPhase = "Strategy questionnaire"
                , explanation =
                    [ El.text "Participants should fill the strategy questionnaire. Once they are done, the experiment is finished."
                    ]
                , startPhaseForm = startPhaseForm
                , required =
                    [ ( "Personality questionnaire completed", qAfterExperiment.personalityAfter )
                    , ( "Strategy questionnaire completed", qAfterExperiment.strategy )
                    ]
                }

        End ->
            [ subTitle "Current phase"
            , title "Experiment finished"
            , El.text "Go see your participants!"
            ]


type alias ExperimenterReadinessConfig =
    { currentPhase : String
    , explanation : List (El.Element Msg)
    , startPhaseForm : Form.Model ()
    , required : List ( String, PProps Bool )
    }


experimenterReadinessView : ExperimenterReadinessConfig -> List (El.Element Msg)
experimenterReadinessView { currentPhase, explanation, startPhaseForm, required } =
    let
        getPReqs p =
            required
                |> List.map (\( text, props ) -> ( text, p props ))

        allReady =
            List.foldr (\( _, { p0, p1 } ) prev -> prev && (p0 && p1))
                True
                required

        startPhaseButton =
            if allReady then
                button (startPhaseForm.status /= Form.Entering)
                    SubmitStartPhase
                    (El.text "Start next phase")

            else
                El.none
    in
    [ subTitle "Current phase"
    , title currentPhase
    , El.paragraph [] explanation
    , El.row [ El.spacing 40, El.centerX ]
        [ playerReadinessView P0 (getPReqs .p0)
        , playerReadinessView P1 (getPReqs .p1)
        ]
    , El.el [ El.centerX ] startPhaseButton
    ]


playerReadinessViewHelp : ( String, Bool ) -> El.Element Msg
playerReadinessViewHelp ( text, ready ) =
    El.row [ Font.size 16 ]
        [ El.text <| text ++ ": "
        , El.el
            [ Font.color <|
                if ready then
                    darkGreen

                else
                    darkGray
            ]
            (El.text <|
                if ready then
                    "yes"

                else
                    "not yet"
            )
        ]


playerReadinessView : P -> List ( String, Bool ) -> El.Element Msg
playerReadinessView p readies =
    El.column
        [ El.spacing 20
        , El.padding 10
        , Border.color lightGray
        , Border.width 1
        , Border.rounded 5
        ]
        ([ El.el [ Font.size 24, El.centerX ] <| El.text <| Utils.pToString p ]
            ++ List.map playerReadinessViewHelp readies
        )



-- PARTICIPANT PHASE VIEWS


connectedParticipantView : P -> Model -> Phase -> ( Bool, List (El.Element Msg) )
connectedParticipantView p { players, startPhaseForm, personForm, personalityForm, awarenessForm, strategyForm, genderDropdown, nationalityDropdown } phase =
    case phase of
        PreExperiment qPreExperiment _ ->
            ( False
            , participantQuestionnaireView
                [ ( Utils.getPProp p qPreExperiment.person, participantPersonView personForm genderDropdown nationalityDropdown )
                , ( Utils.getPProp p qPreExperiment.personalityPre
                  , participantPersonalityView personalityPreText SubmitPersonalityPre personalityForm
                  )
                ]
                afterPreExperimentText
            )

        PreFirstResting ->
            ( False
            , participantQuestionnaireView
                [ ( .ready (Utils.getPProp p players), participantStartPhaseView (Just startPhaseForm) preFirstRestingText )
                ]
                waitRestingText
            )

        Resting duration ->
            ( True
            , ongoingRestingText
            )

        PreTrials ->
            ( False
            , participantQuestionnaireView
                [ ( .ready (Utils.getPProp p players), participantStartPhaseView Nothing preTrialsText )
                ]
                waitTrainingTrialText
            )

        Trial duration config ->
            case config.training of
                Just Visible ->
                    ( False
                    , [ El.row [ El.spacing 50, El.width El.fill, El.moveLeft 150 ]
                            [ El.column [ El.width <| El.px 500, El.spacing 50, El.alignTop ]
                                [ title "Training trial is ongoing"
                                , subTitle "Explore the space and try things out!"
                                , participantLegendView p
                                , participantClickView p players
                                ]
                            , El.el [ El.width <| El.px 600 ] <| El.html <| spaceView config players (Participant p) 1
                            ]
                      ]
                    )

                Just Hidden ->
                    ( True
                    , ongoingTrainingHiddenText
                    )

                Nothing ->
                    ( True
                    , ongoingTrialText
                    )

        AfterResting ->
            ( False
            , participantQuestionnaireView
                [ ( .ready (Utils.getPProp p players), participantStartPhaseView Nothing readinessAfterRestingText )
                ]
                waitTrialText
            )

        AfterTrial training nextPhase qAfterTrial ->
            -- TODO: change the "training nextPhase" pair to a single trialTransition with only allowed values
            -- this will let us remove all the error states in the case blocks below
            let
                ( readiness, readyText ) =
                    case training of
                        Just Visible ->
                            case nextPhase of
                                TrialPhase (Just Visible) ->
                                    ( []
                                    , waitTrainingTrialText
                                    )

                                TrialPhase (Just Hidden) ->
                                    ( [ ( .ready (Utils.getPProp p players), participantStartPhaseView Nothing startTrainingHiddenTrialsText ) ]
                                    , waitTrainingTrialText
                                    )

                                TrialPhase Nothing ->
                                    ( [ ( False, errorPhaseTransitionText ) ], [] )

                                RestingPhase ->
                                    ( [ ( False, errorPhaseTransitionText ) ], [] )

                        Just Hidden ->
                            case nextPhase of
                                TrialPhase (Just Visible) ->
                                    ( [ ( False, errorPhaseTransitionText ) ], [] )

                                TrialPhase (Just Hidden) ->
                                    ( []
                                    , waitTrainingTrialText
                                    )

                                TrialPhase Nothing ->
                                    ( [ ( .ready (Utils.getPProp p players), participantStartPhaseView Nothing startRealTrialsText ) ]
                                    , waitTrialText
                                    )

                                RestingPhase ->
                                    ( [ ( False, errorPhaseTransitionText ) ], [] )

                        Nothing ->
                            case nextPhase of
                                TrialPhase (Just _) ->
                                    ( [ ( False, errorPhaseTransitionText ) ], [] )

                                TrialPhase Nothing ->
                                    ( []
                                    , waitTrialText
                                    )

                                RestingPhase ->
                                    ( [ ( .ready (Utils.getPProp p players), participantStartPhaseView Nothing startRestingText ) ]
                                    , waitRestingText
                                    )
            in
            ( False
            , participantQuestionnaireView
                ([ ( Utils.getPProp p qAfterTrial.awareness, participantAwarenessView awarenessForm ) ] ++ readiness)
                readyText
            )

        AfterExperiment qAfterExperiment ->
            ( False
            , participantQuestionnaireView
                [ ( Utils.getPProp p qAfterExperiment.personalityAfter
                  , participantPersonalityView personalityAfterText SubmitPersonalityAfter personalityForm
                  )
                , ( Utils.getPProp p qAfterExperiment.strategy, participantStrategyView strategyForm )
                ]
                endText
            )

        End ->
            ( False
            , endText
            )


genderDropdownConfig : Dropdown.Config String Msg (Form.Model Person)
genderDropdownConfig =
    let
        containerAttrs =
            [ El.width El.shrink, El.pointer ]

        selectAttrs =
            [ Border.width 1
            , Border.rounded 5
            , El.paddingXY 16 8
            , El.spacing 10
            , El.width El.fill
            ]

        listAttrs =
            [ Border.width 1
            , Border.rounded 5
            , El.width El.fill
            , El.paddingXY 0 5
            , Background.color white
            ]

        itemToPrompt item =
            El.text item

        itemToElement selected highlighted item =
            El.el
                [ Background.color <|
                    if highlighted then
                        lightBlue

                    else if selected then
                        lightGreen

                    else
                        white
                , El.width El.fill
                , El.paddingXY 16 8
                , El.mouseOver [ Background.color lightGray ]
                ]
                (El.text item)
    in
    Dropdown.basic
        { itemsFromModel = always genderOptions
        , selectionFromModel = .input >> .gender
        , dropdownMsg = PersonGenderDropdownMsg
        , onSelectMsg = PersonSelectGender
        , itemToPrompt = itemToPrompt
        , itemToElement = itemToElement
        }
        |> Dropdown.withContainerAttributes containerAttrs
        |> Dropdown.withSelectAttributes selectAttrs
        |> Dropdown.withListAttributes listAttrs


nationalityDropdownConfig : Dropdown.Config String Msg (Form.Model Person)
nationalityDropdownConfig =
    let
        containerAttrs =
            [ El.width (El.fill |> El.minimum 300), El.pointer ]

        selectAttrs =
            [ Border.width 1
            , Border.rounded 5
            , El.paddingXY 16 8
            , El.spacing 10
            , El.width El.fill
            ]

        searchAttrs =
            [ Border.width 0, El.padding 0 ]

        listAttrs =
            [ Border.width 1
            , Border.rounded 5
            , El.width El.shrink
            , El.paddingXY 0 5
            , Background.color white
            ]

        itemToPrompt item =
            El.text item

        itemToElement selected highlighted item =
            El.el
                [ Background.color <|
                    if highlighted then
                        lightBlue

                    else if selected then
                        lightGreen

                    else
                        white
                , El.width El.fill
                , El.paddingXY 16 8
                , El.mouseOver [ Background.color lightGray ]
                ]
                (El.text item)
    in
    Dropdown.filterable
        { itemsFromModel = always nationalityOptions
        , selectionFromModel = .input >> .nationality
        , dropdownMsg = PersonNationalityDropdownMsg
        , onSelectMsg = PersonSelectNationality
        , itemToPrompt = itemToPrompt
        , itemToElement = itemToElement
        , itemToText = identity
        }
        |> Dropdown.withContainerAttributes containerAttrs
        |> Dropdown.withSelectAttributes selectAttrs
        |> Dropdown.withListAttributes listAttrs
        |> Dropdown.withSearchAttributes searchAttrs


participantPersonView : Form.Model Person -> Dropdown.State String -> Dropdown.State String -> List (El.Element Msg)
participantPersonView personForm genderDropdown nationalityDropdown =
    let
        input =
            personForm.input

        status =
            personForm.status

        complete =
            -- TODO: use Validate here too?
            input.age /= Nothing && input.gender /= Nothing && input.nationality /= Nothing
    in
    [ El.textColumn [ El.spacing 20 ] personText
    , Input.text
        [ El.padding 10
        , El.spacing 20
        , El.width <| El.px 80
        ]
        { onChange = PersonInputAge
        , text =
            case input.age of
                Nothing ->
                    ""

                Just age ->
                    String.fromInt age
        , placeholder = Nothing
        , label = Input.labelLeft [] (El.text ageText)
        }
    , El.row [ El.spacing 20 ]
        [ El.el [] <| El.text "What is your gender?"
        , Dropdown.view genderDropdownConfig personForm genderDropdown
        ]
    , El.row [ El.spacing 20, El.width El.fill ]
        [ El.el [] <| El.text "What is your nationality?"
        , Dropdown.view nationalityDropdownConfig personForm nationalityDropdown
        ]
    , if complete then
        button (status /= Form.Entering)
            SubmitPerson
            (El.text "Submit")

      else
        El.text "Please answer all the questions to move on to the next phase"
    ]


participantPersonalityQuestionView : Int -> String -> Maybe Int -> El.Element Msg
participantPersonalityQuestionView id questionText maybeAnswer =
    let
        optionEl text =
            El.el [ Font.size 16 ] (El.text text)
    in
    Input.radioRow
        [ El.padding 10
        , El.spacing 20
        ]
        { onChange = PersonalityInput id
        , selected = maybeAnswer
        , label = Input.labelAbove [] (El.text questionText)
        , options =
            [ Input.option 1 (optionEl "Disagree strongly")
            , Input.option 2 (optionEl "Disagree a little")
            , Input.option 3 (optionEl "Neutral; no opinion")
            , Input.option 4 (optionEl "Agree a little")
            , Input.option 5 (optionEl "Agree strongly")
            ]
        }


participantPersonalityView : List (El.Element Msg) -> Msg -> Form.Model Personality -> List (El.Element Msg)
participantPersonalityView introText msg { input, status } =
    let
        complete =
            personalityQuestionTexts
                |> Dict.map (\id _ -> Dict.member id input)
                |> Dict.foldl (\_ answered sofar -> answered && sofar) True

        questionViews =
            personalityQuestionTexts
                |> Dict.map
                    (\i q ->
                        participantPersonalityQuestionView
                            i
                            q
                            (Dict.get i input |> Maybe.map Tuple.second)
                    )
                |> Dict.foldr (\_ qView sofar -> qView :: sofar) []
    in
    [ El.textColumn [ El.spacing 20 ] (introText ++ personalityCharacteristicsText)
    , subTitle "I am someone who..."
    , El.column [ El.spacing 30 ] questionViews
    , if complete then
        button (status /= Form.Entering) msg (El.text "Submit")

      else
        El.text "Please answer all the questions to move on to the next phase"
    ]


participantAwarenessView : Form.Model Awareness -> List (El.Element Msg)
participantAwarenessView { input, status } =
    let
        complete =
            input.pas /= Nothing
    in
    [ pasTitleText
    , Input.radio
        [ El.padding 10
        , El.spacing 20
        ]
        { onChange = AwarenessInput
        , selected = input.pas
        , label = Input.labelAbove [] (El.paragraph [] [ El.text awarenessPasText ])
        , options =
            [ Input.option 0 (El.text "I did not press the button")
            , Input.option 1 (El.text "No experience")
            , Input.option 2 (El.text "Vague impression")
            , Input.option 3 (El.text "Almost clear experience")
            , Input.option 4 (El.text "Clear experience")
            ]
        }
    , if complete then
        button (status /= Form.Entering)
            SubmitAwareness
            (El.text "Submit")

      else
        El.text "Please answer the question to move on to the next phase"
    ]


participantStrategyView : Form.Model Strategy -> List (El.Element Msg)
participantStrategyView { input, status } =
    [ strategyCommentIntroText
    , Input.multiline
        [ El.height (El.minimum (3 * 44) El.shrink) ]
        { onChange = StrategySelfInput
        , text = input.self
        , placeholder = Nothing
        , label = Input.labelAbove [] (El.textColumn [ El.paddingXY 0 10, El.spacing 20 ] strategySelfText)
        , spellcheck = False
        }
    , Input.multiline
        [ El.height (El.minimum (3 * 44) El.shrink) ]
        { onChange = StrategyOtherInput
        , text = input.other
        , placeholder = Nothing
        , label = Input.labelAbove [] (El.textColumn [ El.paddingXY 0 10, El.spacing 20 ] strategyOtherText)
        , spellcheck = False
        }
    , Input.multiline
        []
        { onChange = StrategyCommentInput
        , text = input.comment
        , placeholder = Nothing
        , label = Input.labelAbove [] (El.textColumn [ El.paddingXY 0 10, El.spacing 20 ] commentText)
        , spellcheck = False
        }
    , button (status /= Form.Entering)
        SubmitStrategy
        (El.text "Submit")
    ]


participantQuestionnaireView : List ( Bool, List (El.Element Msg) ) -> List (El.Element Msg) -> List (El.Element Msg)
participantQuestionnaireView steps finalText =
    let
        remainingSteps =
            List.filterMap
                (\( done, v ) ->
                    if done then
                        Nothing

                    else
                        Just v
                )
                steps
    in
    case remainingSteps of
        stepView :: _ ->
            stepView

        [] ->
            finalText


participantStartPhaseView : Maybe (Form.Model ()) -> List (El.Element Msg) -> List (El.Element Msg)
participantStartPhaseView maybeForm text =
    let
        submit =
            case maybeForm of
                Nothing ->
                    subTitle "Long-press the controller's button to move on."

                Just form ->
                    button (form.status /= Form.Entering)
                        SubmitReady
                        (El.text "I am ready")
    in
    [ El.textColumn [ El.spacing 40 ] text
    , submit
    ]



-- VIRTUAL SPACE


playerView : SpaceConfig -> Player -> Float -> S.Svg msg
playerView config p zoom =
    S.svg
        [ SA.width "150px"
        , SA.height "auto"
        , SA.viewBox "0 0 300 300"
        ]
        [ S.circle
            [ SA.cx "150"
            , SA.cy "150"
            , SA.r "100"
            , SA.fill "transparent"
            , SA.stroke "#aaa"
            , SA.strokeWidth <| String.fromFloat (0.5 * zoom)
            ]
            []
        , spacePlayerView config p0Color p False zoom
        , spaceButtonView p0Color p zoom
        ]


p0Color : String
p0Color =
    "#ffac05ff"


p1Color : String
p1Color =
    "#b45160ff"


spaceView : SpaceConfig -> Players -> Slot -> Float -> S.Svg msg
spaceView config { p0, p1 } slot zoom =
    let
        ( p0ShowButton, p1ShowButton ) =
            case slot of
                Experimenter ->
                    ( True, True )

                Participant P0 ->
                    ( not p0.clicked, False )

                Participant P1 ->
                    ( False, not p1.clicked )
    in
    S.svg
        [ SA.width "100%"
        , SA.height "auto"
        , SA.viewBox "40 40 260 260"
        ]
        [ S.circle
            [ SA.cx "150"
            , SA.cy "150"
            , SA.r "100"
            , SA.fill "transparent"
            , SA.stroke "#aaa"
            , SA.strokeWidth <| String.fromFloat (0.5 * zoom)
            ]
            []
        , spacePlayerView config p0Color p0 p0ShowButton zoom
        , spaceShadowView config p0Color (p0.x + config.shadowDelta0) zoom
        , spaceStaticView config p0Color config.static0 zoom
        , spacePlayerView config p1Color p1 p1ShowButton zoom
        , spaceShadowView config p1Color (p1.x + config.shadowDelta1) zoom
        , spaceStaticView config p1Color config.static1 zoom
        ]


experimenterLegendView : Players -> El.Element msg
experimenterLegendView { p0, p1 } =
    let
        clicked =
            "Has clicked."

        notClicked =
            "Click pending."

        items =
            [ ( spacePlayerViewAlone p0Color
              , "P0. "
                    ++ (if p0.clicked then
                            clicked

                        else
                            notClicked
                       )
              )
            , ( spacePlayerViewAlone p1Color
              , "P1. "
                    ++ (if p1.clicked then
                            clicked

                        else
                            notClicked
                       )
              )
            ]
    in
    El.column [ El.spacing 20 ] <|
        [ El.text "Legend:" ]
            ++ List.map legendElementView items


participantLegendView : P -> El.Element msg
participantLegendView p =
    let
        ( pColor, otherColor ) =
            case p of
                P0 ->
                    ( p0Color, p1Color )

                P1 ->
                    ( p1Color, p0Color )

        items =
            [ ( spacePlayerViewAlone pColor, "your avatar" )
            , ( spaceShadowViewAlone pColor, "your shadow" )
            , ( spaceStaticViewAlone pColor, "your static object" )
            , ( spacePlayerViewAlone otherColor, "partner's avatar" )
            , ( spaceShadowViewAlone otherColor, "partner's shadow" )
            , ( spaceStaticViewAlone otherColor, "partner's static object" )
            ]
    in
    El.column [ El.spacing 20 ] <|
        [ El.text "The different objects are as follows:" ]
            ++ List.map legendElementView items


legendElementView : ( S.Svg msg, String ) -> El.Element msg
legendElementView ( svg, text ) =
    El.row [ El.spacing 10 ]
        [ El.el [ El.width <| El.px 20 ] <|
            El.html <|
                S.svg
                    [ SA.width "100%"
                    , SA.height "auto"
                    , SA.viewBox "-4 -4 8 8"
                    ]
                    [ svg ]
        , El.text text
        ]


participantClickView : P -> Players -> El.Element msg
participantClickView p players =
    participantClickViewHelp (pX p players)


participantClickViewHelp : Player -> El.Element msg
participantClickViewHelp p =
    case p.clicked of
        True ->
            El.paragraph [ El.spacing 10 ] <|
                [ El.text "You've pressed your button; no more presses in this trial! Remember to help your partner find you." ]

        False ->
            El.text ""


pX : P -> { p0 : a, p1 : a } -> a
pX p { p0, p1 } =
    case p of
        P0 ->
            p0

        P1 ->
            p1


spaceButtonView : String -> Player -> Float -> S.Svg msg
spaceButtonView color player zoom =
    S.circle
        [ SA.cx "50"
        , SA.cy "50"
        , SA.r "15"
        , SA.fill <|
            if player.button then
                color

            else
                "transparent"
        , SA.stroke color
        , SA.strokeWidth <| String.fromFloat (0.5 * zoom)
        ]
        []


spacePlayerView : SpaceConfig -> String -> Player -> Bool -> Float -> S.Svg msg
spacePlayerView config color player showButton zoom =
    S.g []
        [ spaceObjectViewInSpace config
            { moving = True
            , stroke = color
            , fill = color
            , size = config.avatarWidth * zoom
            , x = player.x
            }
        , spaceObjectViewInSpace config
            { moving = True
            , stroke =
                if player.button && showButton then
                    color

                else
                    "transparent"
            , fill = "transparent"
            , size = config.avatarWidth * zoom * 4
            , x = player.x
            }
        ]


spacePlayerViewAlone : String -> S.Svg msg
spacePlayerViewAlone color =
    spaceObjectViewAlone
        { moving = True
        , stroke = color
        , fill = color
        , size = 4
        , x = 0
        }


spaceShadowView : SpaceConfig -> String -> Float -> Float -> S.Svg msg
spaceShadowView config color x zoom =
    spaceObjectViewInSpace config
        { moving = True
        , stroke = color
        , fill = "transparent"
        , size = config.avatarWidth * zoom
        , x = x
        }


spaceShadowViewAlone : String -> S.Svg msg
spaceShadowViewAlone color =
    spaceObjectViewAlone
        { moving = True
        , stroke = color
        , fill = "transparent"
        , size = 4
        , x = 0
        }


spaceStaticView : SpaceConfig -> String -> Float -> Float -> S.Svg msg
spaceStaticView config color x zoom =
    spaceObjectViewInSpace config
        { moving = False
        , stroke = color
        , fill = "transparent"
        , size = config.staticWidth * zoom
        , x = x
        }


spaceStaticViewAlone : String -> S.Svg msg
spaceStaticViewAlone color =
    spaceObjectViewAlone
        { moving = False
        , stroke = color
        , fill = "transparent"
        , size = 4
        , x = 0
        }


type alias ObjectViewConfig =
    { moving : Bool
    , stroke : String
    , fill : String
    , size : Float
    , x : Float
    }


spaceObjectViewInSpace : SpaceConfig -> ObjectViewConfig -> S.Svg msg
spaceObjectViewInSpace spaceConfig objectViewConfig =
    let
        angle =
            -360 * objectViewConfig.x / spaceConfig.envWidth
    in
    spaceObjectViewHelp
        objectViewConfig
        [ SA.transform <| "rotate(" ++ String.fromFloat angle ++ ", 150, 150) translate(150, 50)" ]


spaceObjectViewAlone : ObjectViewConfig -> S.Svg msg
spaceObjectViewAlone objectViewConfig =
    spaceObjectViewHelp objectViewConfig []


spaceObjectViewHelp : ObjectViewConfig -> List (S.Attribute msg) -> S.Svg msg
spaceObjectViewHelp { moving, stroke, fill, size, x } moreAttrs =
    let
        strokeWidth =
            size / 4

        ( base, attrs ) =
            case moving of
                False ->
                    ( S.rect
                    , [ SA.x <| String.fromFloat <| strokeWidth / 2 - size / 2
                      , SA.y <| String.fromFloat <| strokeWidth / 2 - size / 2
                      , SA.width <| String.fromFloat <| size - strokeWidth
                      , SA.height <| String.fromFloat <| size - strokeWidth
                      ]
                    )

                True ->
                    ( S.circle
                    , [ SA.cx <| String.fromFloat 0
                      , SA.cy <| String.fromFloat 0
                      , SA.r <| String.fromFloat (size / 2 - strokeWidth / 2)
                      ]
                    )
    in
    base
        (attrs
            ++ [ SA.fill fill
               , SA.stroke stroke
               , SA.strokeWidth <| String.fromFloat strokeWidth
               ]
            ++ moreAttrs
        )
        []



-- VIEW UTILS


titleSize : Int
titleSize =
    38


subTitleSize : Int
subTitleSize =
    28


white : El.Color
white =
    El.rgb 1 1 1


black : El.Color
black =
    El.rgb 0 0 0


lightGray : El.Color
lightGray =
    El.rgb255 204 204 204


darkGray : El.Color
darkGray =
    El.rgb255 60 60 60


darkGreen : El.Color
darkGreen =
    El.rgb255 71 121 80


lightGreen : El.Color
lightGreen =
    El.rgb255 205 255 198


lightBlue : El.Color
lightBlue =
    El.rgb255 168 196 213


button : Bool -> msg -> El.Element msg -> El.Element msg
button disabled onPress label =
    let
        attrs =
            if disabled then
                [ Font.color darkGray
                , Border.color (El.rgb255 214 217 221)
                , Background.color (El.rgb255 249 250 251)
                ]

            else
                [ Font.color (El.rgb255 38 92 131)
                , Border.color (El.rgb255 214 217 221)
                , Background.color (El.rgb255 249 250 251)
                , El.mouseOver [ Background.color (El.rgb255 237 240 243) ]
                , El.mouseDown [ Background.color lightGray, Border.color lightGray, Font.color darkGray ]
                ]
    in
    Input.button
        ([ Border.width 1
         , El.padding 8
         ]
            ++ attrs
        )
        { onPress =
            if disabled then
                Nothing

            else
                Just onPress
        , label = label
        }



{-
   input : Bool -> (String -> Msg) -> String -> Input.Label Msg -> El.Element Msg
   input disabled onChange text label =
       inputStyled disabled [] onChange text label
-}


inputStyled : Bool -> List (El.Attribute Msg) -> (String -> Msg) -> String -> Input.Label Msg -> El.Element Msg
inputStyled disabled styles onChange text label =
    let
        disabledStyle =
            if disabled then
                [ Background.color lightGray
                ]

            else
                []
    in
    Input.text (styles ++ disabledStyle)
        { onChange =
            if disabled then
                \_ -> NoOp

            else
                onChange
        , text = text
        , placeholder = Nothing
        , label = label
        }


title : String -> El.Element msg
title text =
    titleStyled [ ( [], text ) ]


titleStyled : List ( List (El.Attribute msg), String ) -> El.Element msg
titleStyled stylesText =
    El.el [ Font.size titleSize ] <|
        El.paragraph []
            (List.map (\st -> El.el (Tuple.first st) (El.text <| Tuple.second st)) stylesText)


subTitle : String -> El.Element msg
subTitle text =
    subTitleStyled [ ( [], text ) ]


subTitleStyled : List ( List (El.Attribute msg), String ) -> El.Element msg
subTitleStyled stylesText =
    El.el [ Font.size subTitleSize ] <|
        El.paragraph []
            (List.map (\st -> El.el (Tuple.first st) (El.text <| Tuple.second st)) stylesText)



{-
   onEnter : msg -> El.Attribute msg
   onEnter msg =
       El.htmlAttribute
           (Html.Events.on "keyup"
               (D.field "key" D.string
                   |> D.andThen
                       (\key ->
                           if key == "Enter" then
                               D.succeed msg

                           else
                               D.fail "Not the enter key"
                       )
               )
           )
-}
-- TEXT


list : List (El.Attribute msg) -> List (El.Element msg) -> El.Element msg
list attrs items =
    El.column attrs <|
        List.map
            (\item -> El.el [ El.htmlAttribute (HA.style "display" "list-item") ] item)
            items


personText =
    [ subTitle "Before we start, we would like to ask a few questions about you and your personality"

    -- TODO: add a time estimate
    ]


personalityPreText =
    [ subTitle "We would now like to ask a few questions about your personality" ]


personalityAfterText =
    [ subTitle "We would now like to ask you once more a few questions about your personality" ]


personalityCharacteristicsText =
    [ El.paragraph [] [ El.text """Here are a number of characteristics that may or may not apply to you.
For example, do you agree that you are someone who likes to spend time with others?
Please indicate the extent to which you agree or disagree with each statement below.""" ]

    -- TODO: add a time estimate
    ]


afterPreExperimentText =
    [ subTitle "Thank you for filling the questionnaire."
    , subTitle "Now, while experimenters are still setting up your cap and electrodes, please:"
    , list
        [ El.paddingEach { top = 0, right = 0, bottom = 0, left = 30 }
        , El.spacing 20
        ]
        [ subTitleStyled
            [ ( [], "Carefully read the " )
            , ( [ Font.italic ], "Perceptual Crossing experiment" )
            , ( [], " instructions you were given on paper at the beginning of the experiment" )
            ]
        , subTitle "If you have any questions, ask the experimenters"
        ]
    ]


preFirstRestingText =
    [ subTitle "We are now going to measure your body and brain activity while you rest."
    , subTitle "Please:"
    , list
        [ El.paddingEach { top = 0, right = 0, bottom = 0, left = 30 }
        , El.spacing 20
        ]
        [ subTitle "Take a few moments to relax"
        , subTitle "Find a comfortable position with your hands on the table"
        , subTitle "When you are ready, click the button below and close your eyes"
        ]
    , subTitle "A tone will signify when the phase is complete."
    ]


preTrialsText =
    [ subTitleStyled
        [ ( [], "We are now going to start the perceptual crossing experiment by doing 3 " )
        , ( [ Font.bold ], "training" )
        , ( [], " trials." )
        ]
    , subTitle "In these trials, you will first see the full virtual space, then it will be hidden. Try things out to understand how the controller works."
    ]


waitTrainingTrialText =
    [ title "Training trial is about to start"
    , subTitle "Just a few moments, we are getting everything ready."
    ]


waitTrialText =
    [ title "Trial is about to start"
    , subTitle "Just a few moments, we are getting everything ready."
    ]


waitRestingText =
    [ title "Please keep your eyes closed and your hands on the table"
    , subTitle "The Resting phase will start in a few moments."
    ]


errorPhaseTransitionText =
    [ title "An error occured"
    , subTitle "We reached an inconsistent phase transition. Please show this to the experimenter."
    ]


readinessAfterRestingText =
    [ subTitle "We are now going back to trials." ]


startTrainingHiddenTrialsText =
    [ subTitleStyled
        [ ( [], "In the next training trial, we are going to " )
        , ( [ Font.bold ], "hide" )
        , ( [], " the virtual space so you get used to the real trials." )
        ]
    ]


startRealTrialsText =
    [ subTitleStyled
        [ ( [], "The training trials are over, we are now going to start the " )
        , ( [ Font.bold ], "real" )
        , ( [], " trials." )
        ]
    ]


ongoingRestingText =
    [ title "Resting phase is ongoing"
    , subTitle "Please keep your eyes closed and your hands on the table."
    ]


ongoingTrainingHiddenText =
    [ title "Training trial is ongoing"
    , subTitle "Please keep your eyes closed."
    , subTitle "Using the controller, find your partner and help them find you."
    ]


ongoingTrialText =
    [ title "Trial is ongoing"
    , subTitle "Please keep your eyes closed."
    , subTitle "Using the controller, find your partner and help them find you."
    ]


strategyCommentIntroText =
    subTitle "Before we finish, we would like to know more about your experience and strategies during the trials."


strategySelfText =
    [ subTitleStyled
        [ ( [], "Describe any strategies " )
        , ( [], "you" )
        , ( [], " developed or used during the trials, with as much detail as possible" )
        ]
    ]


strategyOtherText =
    [ subTitleStyled
        [ ( [], "Describe any strategies you believe " )
        , ( [ Font.bold ], "your partner" )
        , ( [], " developed or used, with as much detail as possible" )
        ]
    ]


commentText =
    [ subTitle "If you faced any difficulties during the experiment, or if you have any other comments, please enter them here; they can help us improve the experiment" ]


endText =
    [ title "The experiment is finished"
    , subTitle "Thank you! An experimenter will come and find you in a few minutes."
    ]


startRestingText =
    [ subTitle "We will now have a resting phase."
    , subTitle "Please:"
    , list
        [ El.paddingEach { top = 0, right = 0, bottom = 0, left = 30 }
        , El.spacing 20
        ]
        [ subTitle "Take a few moments to relax"
        , subTitle "Find a comfortable position with your hands on the table"
        ]
    ]


ageText =
    "How old are you?"


awarenessPasText =
    "How clearly did you experience your partner at the time you pressed the button?"


pasTitleText =
    title "If you pressed the button, did you feel the presence of your partner?"
