#!/usr/bin/env bash
set -e

SCRIPT="$(readlink -f "$0")"
INITIAL_FOLDER="$(pwd)"

cd "$(dirname "$SCRIPT")"
HASH="$(git rev-parse HEAD)"

mkdir -p assets
printf "\n\nGetting elm.js from current commit artifacts (hash %s)\n\n\n" "$HASH"
wget -O assets/elm.js "https://gitlab.com/oist-ecsu/pce-webviz/-/jobs/artifacts/$HASH/raw/assets/elm.js?job=build"
cd "$INITIAL_FOLDER"
